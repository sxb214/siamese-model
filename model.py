import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_lightning import LightningModule, Trainer

from sklearn.model_selection import train_test_split

from transformers import BertModel, AdamW
import numpy as np

##this model should take in 2 bert models, for question and context, and then combine them and add a DNN. 

CUDA = torch.cuda.is_available()
#CUDA = False  #put in for debugging
if CUDA:
    DEVICE = "cuda"
else:
    DEVICE = "cpu"


def DPRLoss(output_logits, batch_size, answer_offset):
    log_softmaxed_output =  F.log_softmax(output_logits, dim = -1) #softmaxes and takes log of output logits

    target_vec = torch.zeros(batch_size) #this vector needs to be sent to device
    target_vec = target_vec.to(DEVICE) #there has to be a better way to do this
   
    for idx in range(len(target_vec)):
        target_vec[idx] = idx * (1 + answer_offset) 
        #assumes interpolated positives and negatives

    loss = F.nll_loss(log_softmaxed_output, target_vec.long())
    return loss

def DPRAcc(output_logits, batch_size, answer_offset):

    argmaxed_output = np.argmax(output_logits.cpu().detach().numpy(), axis = 1)

    target_vec = np.zeros(batch_size)
   
    for idx in range(len(target_vec)):
        target_vec[idx] = idx * (1 + answer_offset) 
        
    diff_vec = target_vec - argmaxed_output
    num_incorrect = np.count_nonzero(diff_vec)

    acc = 1 - (num_incorrect/batch_size)

    return acc

def DPRMRR(output_logits, batch_size, answer_offset):

    sorted_vec, indices = torch.sort(output_logits, descending = True)
    indices_np = indices.cpu().detach().numpy()

    #atVal = (negative_ratio + 1) * batch_size
    
    ranks_vec = np.zeros(batch_size)

    for i in range(batch_size):
        correct_output = i * (1 + answer_offset) 
        
        idx_of_correct_output = np.where(indices_np[i] == correct_output)[0][0]

        ranks_vec[i] = idx_of_correct_output + 1 #since arrays are 0-indexed
    
    mrr = 0
    for elt in ranks_vec:
        mrr += 1/elt

    mrr /= batch_size

    return mrr 

class SiameseModel(LightningModule):
    def __init__(self, q_str, c_str, train_batch_size, val_batch_size, test_batch_size, train_answer_offset, val_answer_offset, test_answer_offset): 
        super(SiameseModel, self).__init__()
        self.QuestionModel = BertModel.from_pretrained(q_str)
        self.ContextModel = BertModel.from_pretrained(c_str)
        self.train_batch_size = train_batch_size
        self.val_batch_size = val_batch_size
        self.test_batch_size = test_batch_size

        self.train_answer_offset = train_answer_offset
        self.val_answer_offset = val_answer_offset
        self.test_answer_offset = test_answer_offset


    def forward(self, batch):

        question_output = self.QuestionModel(**batch[0])
        context_output = self.ContextModel(**batch[1])

        output_matrix = torch.matmul(question_output.pooler_output.squeeze(0), context_output.pooler_output.squeeze(0).T)  
        if (len(output_matrix.shape) == 1):  #allows batch size 1 to work
            output_matrix = output_matrix.unsqueeze(0)

        #return question_output, context_output
        return output_matrix

    def training_step(self, batch, batch_idx):
        
        output_matrix = self.forward(batch)

        batch_loss = DPRLoss(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.train_answer_offset)
        batch_acc = DPRAcc(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.train_answer_offset)
        batch_mrr = DPRMRR(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.train_answer_offset)


        self.log('train_acc', batch_acc, prog_bar = True, on_step = True)
        self.log('train_mrr', batch_mrr, prog_bar = True, on_step = True)


        return batch_loss 
    
    def validation_step(self, batch, batch_idx):


        output_matrix = self.forward(batch)

        val_batch_loss = DPRLoss(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.val_answer_offset)
        val_batch_acc = DPRAcc(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.val_answer_offset)
        val_batch_mrr = DPRMRR(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.val_answer_offset)

        self.log('val_loss', val_batch_loss, prog_bar = True,)
        self.log('val_acc', val_batch_acc, prog_bar = True, )
        self.log('val_mrr', val_batch_mrr, prog_bar = True, )


    def test_step(self, batch, batch_idx):
]        output_matrix = self.forward(batch)

        test_batch_acc = DPRAcc(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.test_answer_offset)
        test_batch_mrr = DPRMRR(output_matrix, batch_size = output_matrix.shape[0], answer_offset = self.test_answer_offset)

        self.log('test_acc', test_batch_acc, prog_bar = True, )
        self.log('test_mrr', test_batch_mrr, prog_bar = True, )

    def configure_optimizers(self):
        opt = AdamW(self.parameters(), lr = 5e-5)
        return opt
