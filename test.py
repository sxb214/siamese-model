from model import SiameseModel
from datamodules import PRMModule, MSMarcoModule

from pytorch_lightning import LightningModule, Trainer, LightningDataModule

from transformers import BertModel, BertTokenizerFast, BertConfig, AdamW, BatchEncoding


q_model_str = 'prajjwal1/bert-tiny'
c_model_str = 'bert-base-uncased'

q_tokenizer = BertTokenizerFast.from_pretrained(q_model_str)
c_tokenizer = BertTokenizerFast.from_pretrained(c_model_str)

data_path = "data/train.jsonl"

datamodule = PRMModule(
    query_tokenizer = q_tokenizer,
    context_tokenizer = q_tokenizer,
    train_negative_ratio = 30,
    train_batch_size = 12,
)



test_model = SiameseModel.load_from_checkpoint(
    q_str = q_model_str, 
    c_str = q_model_str, 
    batch_size = datamodule.train_batch_size, 
    train_answer_offset = datamodule.train_negative_ratio,
    val_answer_offset = datamodule.val_negative_ratio,
    test_answer_offset = datamodule.test_negative_ratio,
    checkpoint_path ="tinybert-msmarco-test-four-long.ckpt"
)


trainer = Trainer(
    gpus=-1, 
    max_epochs = 2, 
    val_check_interval = 300, 
    )


trainer.test(model = test_model, datamodule = datamodule)


#trainer.save_checkpoint('tinybert-msmarco-test-two-more-patient.ckpt')
