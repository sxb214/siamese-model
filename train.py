from model import SiameseModel
from datamodules import MSMarcoModule, PRMModule

from pytorch_lightning import LightningModule, Trainer, LightningDataModule
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from transformers import BertModel, BertTokenizerFast, BertConfig, AdamW, BatchEncoding


tb_model_str = 'prajjwal1/bert-tiny'
bbu_model_str = 'bert-base-uncased'

tb_tokenizer = BertTokenizerFast.from_pretrained(tb_model_str)
bbu_tokenizer = BertTokenizerFast.from_pretrained(bbu_model_str)

data_path = "data/train.jsonl"

datamodule = PRMModule(
    query_tokenizer = bbu_tokenizer,
    context_tokenizer = bbu_tokenizer,
    train_negative_ratio = 6,
    train_batch_size = 2,
)


model = SiameseModel(
    q_str = bbu_model_str, 
    c_str = bbu_model_str, 
    train_batch_size = datamodule.train_batch_size, 
    val_batch_size = datamodule.val_batch_size, 
    test_batch_size = datamodule.test_batch_size,
    train_answer_offset = datamodule.train_negative_ratio,
    val_answer_offset = datamodule.val_negative_ratio,
    test_answer_offset = datamodule.test_negative_ratio

)


early_stop_callback = EarlyStopping(
   monitor='val_mrr',
   min_delta=0.0,
   patience= 5,
   verbose= False ,
   mode='max'
)

trainer = Trainer(
    gpus=-1, 
    #max_epochs = 2, 
    val_check_interval = 400, 
    callbacks=[early_stop_callback]
    )

trainer.fit(model, datamodule)


#test_model = model.load_from_checkpoint(checkpoint_path = "lightning_logs/version_65/checkpoints/epoch=0-step=37824.ckpt", QuestionModel = QuestionModel, ContextModel = ContextModel, tokenizer = tokenizer, dataset = dataset, batch_size =12 )




trainer.save_checkpoint('prm-bert-base-uncased-test-one.ckpt')
