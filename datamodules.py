from pytorch_lightning import LightningDataModule
from torch.utils.data import Dataset, DataLoader

from sklearn.model_selection import train_test_split

import json
from aimlresearch_datasets.main import load_dataset



class TokenizerAndCollator(object):
    def __init__(self, query_tokenizer, context_tokenizer):
        self.query_tokenizer = query_tokenizer
        self.context_tokenizer = context_tokenizer

    def __call__(self, batchList):
        queryList = [batch['query'] for batch in batchList]
        questions_input = self.query_tokenizer(queryList, padding = True, return_tensors = 'pt')

        #print(questions_input)
        positiveList = [batch['positive'] for batch in batchList]
        negativesList = [batch['negatives'] for batch in batchList]

        contextList = []
        for i in range(len(positiveList)):
            contextList.append(positiveList[i])
            contextList += negativesList[i]

        contexts_input = self.context_tokenizer(contextList, padding = True, truncation = True, return_tensors = 'pt', max_length = 512)
        #print(contexts_input)

        #print(contexts_input['input_ids'].shape)
        return questions_input, contexts_input

class PRMDataset(Dataset): #structure is 1(1+bs 2+bs 3+bs)2(4+bs 5+bs 6+bs)...
    def __init__(self, type_str, batch_size, negative_ratio, shuffle = False):
        self.corpus = load_dataset("prm_ir", "corpus", split = "train")
        self.queries = load_dataset("prm_ir", split=type_str)
        self.batch_size = batch_size
        self.negative_ratio = negative_ratio
        self.length = len(self.queries)

        if (batch_size * (negative_ratio+1) > self.length):
            print("ERROR: batch size or negative ratio is too large - oversampling!")

        #have to implemenet something that scrambles up the indices when shuffle = True

    def __len__(self):
        return(self.length)
    
    def __getitem__(self, idx):  
        #get query string
        #get positive string
        #get nr negative strings
        #return dictionary with elements "query", "positive", "negatives"
        query_str = self.queries[idx]["query"]
        pos_idx = self.queries[idx]["context_index"]
        pos_str = self.corpus[pos_idx]["text"]

        neg_query_idxs = []
        for i in range((idx + self.batch_size), (idx + self.batch_size + self.negative_ratio)):
            if i >= self.length:
                neg_query_idxs.append(i % self.length)
            else:
                neg_query_idxs.append(i)
        #WE END UP SAMPLING THE TEST SET :( can fix by using consecutive values in queries rather than corpus

        #have to implement modulo logic here as well, so as to not break everything
        neg_corpus_idxs = [self.queries[i]["context_index"] for i in neg_query_idxs]

        neg_list = [self.corpus[i]["text"] for i in neg_corpus_idxs]

        return {"query":query_str, "positive":pos_str, "negatives":neg_list}




class PRMModule(LightningDataModule):
    def __init__(self, query_tokenizer, context_tokenizer, train_negative_ratio, train_batch_size):  #for now, val & test are both going to have batch_size full dataset and negative_ratio = 0
        super().__init__()
        self.train_batch_size = train_batch_size
        self.train_negative_ratio = train_negative_ratio
        self.val_batch_size = 178 #length of val set
        self.val_negative_ratio = 0
        self.test_batch_size = 372 #length of test set
        self.test_negative_ratio = 0

        self.tokenizer_and_collator = TokenizerAndCollator(query_tokenizer, context_tokenizer)

    def setup(self, stage = None):
        self.train_dataset = PRMDataset("train", self.train_batch_size, self.train_negative_ratio)
        self.val_dataset = PRMDataset("val", self.val_batch_size, self.val_negative_ratio)
        self.test_dataset = PRMDataset("test", self.test_batch_size, self.test_negative_ratio)

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size = self.train_batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size = self.val_batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size = self.test_batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)




class MSMarcoDataset(Dataset): #15 negatives per positive, and structure is positive then 15 negatives
    def __init__(self, file_path):
        f = open(file_path)
        self.lineList = [json.loads(line) for line in f if (len((json.loads(line))["negatives"]) == 15)] #want to remove examples where there are no negatives
        self.negative_ratio = 15

    def __len__(self):
        return len(self.lineList)
    
    def __getitem__(self, idx):
        return self.lineList[idx]




class MSMarcoModule(LightningDataModule):
    def __init__(self, data_path, query_tokenizer, context_tokenizer, batch_size):
        super().__init__()
        self.data_path = data_path
        self.batch_size = batch_size
        self.negative_ratio = 15
        self.tokenizer_and_collator = TokenizerAndCollator(query_tokenizer, context_tokenizer)


    def setup(self, stage = None):
        self.full_dataset = MSMarcoDataset(self.data_path)
        train_dataset, self.test_dataset = train_test_split(self.full_dataset, test_size = 0.2, train_size = 0.8, random_state = 0 )
        self.train_dataset, self.val_dataset = train_test_split(train_dataset, test_size = 0.05, train_size = 0.95, random_state = 0 )

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size = self.batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size = self.batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size = self.batch_size, collate_fn = self.tokenizer_and_collator, num_workers = 8)

